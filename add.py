import base64
import requests
from random import randint
import sys
from optparse import OptionParser


class Qnap(object):
    username = 'admin'
    password = ''
    protocol = 'https'
    ip = ''
    port = '443'
    use_proxy = 'off'
    proxies = {'http': 'http://127.0.0.1:8888',
               'https': 'http://127.0.0.1:8888'}
    sid = ''
    token = ''
    downloadstation_dir = {
        'temp': 'Download',
        'move': 'Download',
    }

    def __init__(self):
        self.cookies = requests.cookies.RequestsCookieJar()

    def set_username(self, username):
        self.username = username

    def set_password(self, password):
        self.password = password

    def set_protocol(self, protocol):
        self.protocol = protocol

    def set_ip(self, ip):
        self.ip = ip

    def set_port(self, port):
        self.port = port

    def set_sid(self, sid):
        self.sid = sid

    def set_token(self, token):
        self.token = token

    def set_download_temp(self, temp):
        self.downloadstation_dir['temp'] = temp

    def set_download_move(self, move):
        self.downloadstation_dir['move'] = move

    def enable_proxy(self, enabled='off'):
        if enabled.strip().lower() == 'on':
            self.use_proxy = 'on'
        else:
            self.use_proxy = 'off'

    def http(self, method='GET', url=None, json=None, data=None):
        session = requests.Session()
        session.headers.update({'User-Agent': None})
        if self.use_proxy == 'on':
            if hasattr(self, 'proxies'):
                session.proxies.update(self.proxies)
            else:
                print('\t Enabled PROXY but you do not defined proxy server.')
        else:
            pass
        resp = session.request(method, url, verify=False,
                               cookies=self.cookies, data=data, json=json)
        self.cookies.update(resp.cookies)
        return resp

    def http_get(self, sub_url=None, data=None, json=None):
        url = '%s://%s:%s%s' % (self.protocol, self.ip, self.port, sub_url)
        return self.http('GET', url=url, data=data, json=json)

    def http_post(self, sub_url=None, data=None, json=None):
        url = '%s://%s:%s%s' % (self.protocol, self.ip, self.port, sub_url)
        return self.http('POST', url=url, data=data, json=json)

    def downloadstation_login(self):
        payload = {
            'user': self.username,
            'pass': base64.b64encode(bytes(self.password, 'utf8')),
            # 'r': '0.' + str(randint(1000000000000000, 9999999999999999))
        }
        resp = self.http_post(
            sub_url='/downloadstation/V4/Misc/Login', data=payload)
        dict_obj = resp.json()
        try:
            self.set_sid(dict_obj['sid'])
            self.set_token(dict_obj['token'])
        except:
            print('\t 帳號/密碼 錯誤?!')
        return resp

    def downloadstation_logout(self):
        payload = {
            'sid': self.sid,
        }
        return self.http_post(sub_url='/downloadstation/V4/Misc/Logout', data=payload)

    def downloadstation_addurl(self, url):
        payload = {
            'temp': self.downloadstation_dir['temp'],
            'move': self.downloadstation_dir['move'],
            'url': url,
            'sid': self.sid,
        }
        return self.http_post(sub_url='/downloadstation/V4/Task/AddUrl', data=payload)

    def downloadstation_query(self):
        payload = {
            'from': '0',
            'limit': '50',
            'type': 'all',
            'status': 'all',
            'sid':	self.sid,
        }
        resp = self.http_post(
            sub_url='/downloadstation/V4/Task/Query', data=payload)
        dict_obj = resp.json()
        try:
            print('Total %s (downloading: %s, completed: %s)' % (
                dict_obj['total'], dict_obj['status']['downloading'], dict_obj['status']['completed']))
            for data in dict_obj['data']:
                print('來源: %s, 建立時間: %s, 完成時間: %s' %
                      (data['source'], data['create_time'], data['finish_time']))
        except:
            print('\t 尚未登入成功?!')
        return resp


def main():
    if len(sys.argv) < 2:
        print('Usage: %s <download_link>' % sys.argv[0])
        sys.exit(1)
    url = sys.argv[1]
    qnap = Qnap()
    qnap.enable_proxy('off')
    qnap.set_username('admin')
    qnap.set_password('admin')
    qnap.set_protocol('http')
    qnap.set_ip('192.168.0.2')
    qnap.set_port('8080')
    qnap.set_download_temp('Download')
    qnap.set_download_move('Download')
    resp = qnap.downloadstation_login()
    print('text is %s' % resp.text)
    resp = qnap.downloadstation_addurl(url)
    print('text is %s' % resp.text)
    resp = qnap.downloadstation_query()
    print('text is %s' % resp.text)
    resp = qnap.downloadstation_logout()
    print('text is %s' % resp.text)


if __name__ == '__main__':
    main()
